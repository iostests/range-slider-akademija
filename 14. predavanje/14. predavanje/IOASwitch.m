//
//  IOASwitch.m
//  14. predavanje
//
//  Created by Danilo Vladusic on 11/3/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

#import "IOASwitch.h"

@interface IOASwitch ()

@property (strong, nonatomic) UIView *thumbView;
@property (nonatomic) CAGradientLayer *gradient;

@end

@implementation IOASwitch


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        [self commonInit];
    }
    return self;
}


- (void)commonInit {
    
    self.offColor = [UIColor grayColor];
    self.thumbColor = [UIColor darkGrayColor];
    self.borderColor = [UIColor darkGrayColor];
    self.isON = YES;
    self.thumbView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    [self addSubview:self.thumbView];
    [self customInit];
    
}

- (void)drawRect:(CGRect)rect { //??? zasto override
    [self customInit];
}

- (void)setNeedsLayout {
    
    [super setNeedsLayout];
    [self setNeedsDisplay];
    
}

- (void)layoutSubviews { //??? zasto override
    
    [super layoutSubviews];
    [self customInit];
}

-(UIColor *)onColor {
    
    if (_onColor) {
        
        return _onColor;
        
    } else {
        
        return [UIColor greenColor];
    }
    
}

- (void)prepareForInterfaceBuilder {
    
    [self customInit];
}

- (void)customInit {
    
    if (!self.thumbView) {
        self.thumbView = [[UIView alloc] initWithFrame:CGRectZero];
        [self addSubview:self.thumbView];
    }
    self.thumbView.userInteractionEnabled = NO;
    
    [self.thumbView setFrame:CGRectMake(self.isON ? self.borderWidth : self.frame.size.width - self.frame.size.height + self.borderWidth, self.borderWidth, self.frame.size.height- 2*self.borderWidth, self.frame.size.height- 2*self.borderWidth)];
    
    [self.thumbView setBackgroundColor:self.thumbColor];
    self.thumbView.layer.cornerRadius = self.thumbView.frame.size.width/2;
    self.thumbView.layer.masksToBounds = YES;
    
    
    self.layer.cornerRadius = self.frame.size.height/2;
    [self.layer setMasksToBounds:YES];
    self.layer.borderWidth = self.borderWidth;
    self.layer.borderColor = self.borderColor.CGColor;

    if (!self.gradient) {
        self.gradient =  [CAGradientLayer layer];
        [self.layer insertSublayer:self.gradient atIndex:0];
        
    }
    self.gradient.frame = self.bounds;
    
    self.gradient.colors = self.isON ? [NSArray arrayWithObjects:(id)[self.onColor CGColor],(id)[self.onColor CGColor], (id)[self.onColor CGColor], nil] : [NSArray arrayWithObjects:(id)[self.offColor CGColor],(id)[self.offColor CGColor], (id)[self.offColor CGColor], nil] ;
    
    self.gradient.endPoint = CGPointMake(0.5, 1);
    
    [self setNeedsDisplay];
}

-(void)setIsON:(BOOL)isON {
    
    _isON = isON;
    
    //Control value has changed, let's notify that
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    
    //Redraw
    [self setNeedsDisplay];
    
    
}

#pragma mark - UIControl

/** Tracking is started **/
-(BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    [super beginTrackingWithTouch:touch withEvent:event];
    
    return YES;
}

-(BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    [super continueTrackingWithTouch:touch withEvent:event];
    
    //Get touch location
    CGPoint lastPoint = [touch locationInView:self];
    CGPoint thumbCenter = self.thumbView.center;
    
    if (lastPoint.x >= self.frame.size.width/2) {
        
        thumbCenter.x = MIN(self.frame.size.width - self.frame.size.height/2, lastPoint.x);

        NSLog(@">>>>>>>>>>");
        
    } else {
        
        thumbCenter.x = MAX(self.frame.size.height/2, lastPoint.x);
        NSLog(@"<<<<<<<<<<");

    }
    
    self.thumbView.center = thumbCenter;

    return YES;
}


-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event{
    [super endTrackingWithTouch:touch withEvent:event];
    
    //Get touch location
    CGPoint lastPoint = [touch locationInView:self];
    
    if (lastPoint.x > self.frame.size.width/2) {
        
        self.isON = NO;
        
    } else {
        
        self.isON = YES;
        
    }

}

@end
