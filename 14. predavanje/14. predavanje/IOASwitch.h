//
//  IOASwitch.h
//  14. predavanje
//
//  Created by Danilo Vladusic on 11/3/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface IOASwitch : UIControl

@property (nonatomic) IBInspectable UIColor *onColor;
@property (nonatomic) IBInspectable UIColor *offColor;
@property (nonatomic) IBInspectable UIColor *thumbColor;
@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable NSInteger borderWidth;
@property (nonatomic) IBInspectable BOOL isON;

@end
