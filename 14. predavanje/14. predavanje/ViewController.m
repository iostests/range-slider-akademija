//
//  ViewController.m
//  14. predavanje
//
//  Created by Danilo Vladusic on 11/3/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

#import "ViewController.h"
#import "IOASwitch.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet IOASwitch *customActionSwitch;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.customActionSwitch addTarget:self action:@selector(nekaTrecaAkcija:) forControlEvents:UIControlEventValueChanged];
    
}

- (IBAction)seconSwitchValueCnaged:(id)sender {
    
    NSLog(@"second switch changed value");

}

- (IBAction)switchChangedValue:(IOASwitch *)sender {
    
    NSLog(@"switch changed value");
}

- (IBAction)touchDown:(IOASwitch *)sender {
    
    NSLog(@"touch down switch changed value");

}

- (void)nekaTrecaAkcija:(IOASwitch *)ioaswitch {
    
    
    NSLog(@"neka treca akcija");

}



@end
